const router = require("express").Router();
const Vigenere = require('caesar-salad').Vigenere;

router.post("/", (req, res) => {
    if (!req.body.password) {
        return res.sendStatus(404)
    } else {
        const decodeWord = Vigenere.Decipher(req.body.password).crypt(req.body.message)
        res.send({"decoded": decodeWord});
    }
})

module.exports = router;