const router = require("express").Router();
const Vigenere = require('caesar-salad').Vigenere;

router.post("/", (req, res) => {
    console.log("yes");
    if (!req.body.password) {
        return res.sendStatus(404)
    } else {
        const encodeWord = Vigenere.Cipher(req.body.password).crypt(req.body.message)
        res.send({"encoded": encodeWord});
    }
})

module.exports = router;