const express = require("express");
const encode = require("./app/encode");
const decode = require("./app/decode");
const cors = require("cors");
const app = express();
const port = 8000;

app.use(cors());
app.use(express.json());
app.use("/encode", encode);
app.use("/decode", decode);

app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
});